from api_helpers import *

# Get epoch times here: https://www.unixtimestamp.com/index.php

start = 1602720000
end = 1610668800

extract_comments(file_name="globaloffensive_raw", subreddit="globaloffensive",
    start_utc=start, end_utc=end)

# extract_comments(file_name="csgo_comments", subreddit="globaloffensive",
#     start_utc=nov_01_2020, end_utc=now, new=True, cache_limit=200)