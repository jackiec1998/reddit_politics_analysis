import pandas as pd

def remove_bad_data(df, columns):

    return df[columns].loc[
        (df['body'] != '[removed]') &
        (df['body'] != '[deleted]') &
        (df['body'].str.lower() != 'nan') &
        (~df['body'].str.contains('I am a bot', na=False))
    ]